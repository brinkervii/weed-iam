venv:
	virtualenv venv
	./venv/bin/python -m pip install pip-tools

requirements.txt: venv
	./venv/bin/python -m piptools compile pyproject.toml --resolver=backtracking -o requirements.txt

install-requirements: venv requirements.txt
	./venv/bin/python -m pip install -r requirements.txt

clean:
	rm -rf build
	rm -rf dist

dist: venv
	./venv/bin/python -m PyInstaller -n weed_iam -F weed_iam/__main__.py
	cp -frax templates dist/templates
	cp -frax static dist/static

.PHONY: install-requirements dist