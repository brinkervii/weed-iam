FROM python:3.11-bookworm AS builder

ENV PYTHONPATH=/build

COPY . /build
WORKDIR /build

RUN apt-get update && apt-get install -y virtualenv
RUN virtualenv -p $(which python3) venv && ./venv/bin/python -m pip install -r requirements.txt -r requirements.dev.txt
RUN make clean dist

FROM python:3.11-bookworm AS app

COPY --from=builder /build/dist /app
WORKDIR /app

RUN mkdir -p /data/db

ENV DB_DIR=/data/db
ENV HOST=0.0.0.0
ENV PORT=8000

EXPOSE 8000

CMD ["/app/weed_iam"]