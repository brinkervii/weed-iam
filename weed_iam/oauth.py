import base64
import json
import sys
import time
from dataclasses import dataclass, field
from enum import Enum
from typing import List, Dict
from typing import TypedDict, Optional

from pydantic import BaseModel

from weed_iam.service_config import current_config
from weed_iam.util import crypto_symmetric
from weed_iam.util.random_fun import make_random_alphanumeric_string

grant_types = ["code", "refresh_token", "access_token"]


class OAuthScope(Enum):
    identity = "id"


class AuthorizationRequestModel(BaseModel):
    response_type: str
    client_id: str
    redirect_uri: str
    scope: List[OAuthScope]
    state: str


all_oauth_scopes = [e for e in OAuthScope]
all_oauth_scope_strings = [e.value for e in OAuthScope]

OAUTH_CODE_SECRET = make_random_alphanumeric_string(16)


class CodeClaims(TypedDict):
    sub: str  # User email
    rng: int  # Salt
    sec: str  # Secret
    iat: int  # Time
    exp: int  # Expire

class RefreshTokenClaims(TypedDict):
    sub: str
    iat: int
    exp: int
    iss: str
    typ: str


class AccessTokenClaims(TypedDict):
    sub: str
    iat: int
    exp: int
    iss: str
    typ: str


def make_code(claims: CodeClaims) -> str:
    json_bytes = json.dumps(claims).encode(sys.getdefaultencoding())
    encrypted_bytes = crypto_symmetric.encrypt(json_bytes, current_config.ephemeral_symmetric_cryptography_key)
    code = base64.b64encode(encrypted_bytes).decode(sys.getdefaultencoding())

    return code


def unmake_code(code: str, remove_from_flight: bool = False) -> Optional[CodeClaims]:
    if remove_from_flight and (code in codes_in_flight):
        codes_in_flight.pop(code)

    encrypted_bytes = base64.b64decode(code)
    plain_bytes = crypto_symmetric.decrypt(encrypted_bytes, current_config.ephemeral_symmetric_cryptography_key)
    json_string = plain_bytes.decode(sys.getdefaultencoding())

    claims: CodeClaims = json.loads(json_string)

    if claims["sec"] != OAUTH_CODE_SECRET:
        return None  # Invalid Secret

    if int(time.time()) > claims["exp"]:
        return None  # Expired

    return claims


@dataclass
class CodeInFlight:
    request: AuthorizationRequestModel
    claims: CodeClaims
    expire: int = field(default_factory=lambda: int(time.time()) + 1800)

    @property
    def is_expired(self):
        return int(time.time()) > self.expire


codes_in_flight: Dict[str, CodeInFlight] = {}


def fly_code(request: AuthorizationRequestModel, claims: CodeClaims):
    code = make_code(claims)
    codes_in_flight[code] = CodeInFlight(request, claims)

    return code


def clean_codes_in_flight():
    remove = []

    for k, code in codes_in_flight.items():
        if code.is_expired:
            remove.append(k)

    for k in remove:
        codes_in_flight.pop(k)
