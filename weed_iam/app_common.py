import base64
import logging
from datetime import datetime
from typing import Annotated, Dict, Optional, Any
from typing import List, Tuple

from fastapi import Depends, HTTPException, Request
from fastapi import Response
from fastapi.templating import Jinja2Templates

from weed_iam import rbac
from weed_iam.rbac import Permission
from weed_iam.repositories.apps_repository import AppsRepository, TomlAppsRepository
from weed_iam.repositories.user_repository import UserRepository, TomlUserRepository, CookieClaims, UserEntity
from weed_iam.service_config import current_config
from weed_iam.util import crypto_symmetric

log = logging.getLogger(__name__)

templates = Jinja2Templates(directory="templates")

user_repository: UserRepository = TomlUserRepository(current_config.user_db_path)
apps_repository: AppsRepository = TomlAppsRepository(current_config.apps_db_path)


def make_redirect_string(request: Request, encoding: str = "UTF-8"):
    redirect = str(request.url)[len(str(request.base_url)):].lstrip("/")
    redirect = redirect.encode(encoding)
    redirect = crypto_symmetric.encrypt(redirect, current_config.ephemeral_symmetric_cryptography_key)
    redirect = base64.b64encode(redirect).decode(encoding)

    return redirect


def unmake_redirect_string(redirect: str, encoding: str = "UTF-8"):
    redirect = base64.b64decode(redirect.encode(encoding))
    redirect = crypto_symmetric.decrypt(redirect, current_config.ephemeral_symmetric_cryptography_key)
    redirect = redirect.decode(encoding)

    return redirect


def redirect_to_login(request: Request):
    raise HTTPException(
        status_code=302,
        detail="Forbidden / Session timed out",
        headers={
            "Location": f"/login?redirect={make_redirect_string(request)}"
        }
    )


async def jwt_auth(request: Request, response: Response):
    token = request.cookies.get(current_config.session_cookie_name)
    ok, claims = await user_repository.session_token_is_valid(token)

    if ok:
        new_session = await user_repository.extend_session_token(token)
        if new_session is not None:
            response.set_cookie(key=current_config.session_cookie_name, value=new_session)

        return claims

    else:
        response.delete_cookie(current_config.session_cookie_name)
        redirect_to_login(request)


UserPermissions = Tuple[CookieClaims, List[Permission], UserEntity]


async def user_permissions(request: Request, response: Response) -> UserPermissions:
    claims = await jwt_auth(request, response)
    user = await user_repository.find_user(claims["sub"])

    if not user:
        redirect_to_login(request)

    permissions = set()

    for role in user["roles"]:
        for permission in rbac.roles[role].permissions:
            permissions.add(permission)

    return claims, list(permissions), user


async def verify_cookie(request: Request, response: Response):
    token = request.cookies.get(current_config.session_cookie_name)

    user = None
    ok, claims = await user_repository.session_token_is_valid(token)

    if ok:
        new_session = await user_repository.extend_session_token(token)
        if new_session is not None:
            response.set_cookie(key=current_config.session_cookie_name, value=new_session)

        user = await user_repository.find_user(claims["sub"])

    return user is not None


JWTAuth = Annotated[CookieClaims, Depends(jwt_auth)]
IsLoggedIn = Annotated[bool, Depends(verify_cookie)]
AuthPermissions = Annotated[UserPermissions, Depends(user_permissions)]


def require_permissions(auth: AuthPermissions, permissions: List[Permission]):
    access_list = list(map(lambda p: p in auth[1], [Permission.use_app, *permissions]))

    log.debug(
        f"Access list for {auth[0]} and required permissions {permissions}:\n:{access_list}\nUser access: {auth[1]}")

    if not all(access_list):
        raise HTTPException(status_code=403, detail="Not Authorized")


def template_context(
        request: Request,
        title: Optional[str] = None,
        auth: Optional[AuthPermissions] = None,
        is_logged_in: Optional[bool] = False,
        data: Optional[Dict[str, Any]] = None
):
    import builtins as py

    user: Optional[UserEntity] = None
    if auth and auth[2]:
        user = auth[2]

    return {
        "request": request,
        "title": title or "Weed IAM",
        "is_logged_in": is_logged_in or auth is not None,
        "form": {},
        **(data or {}),
        "user": user,
        "Permission": Permission,
        "has_permission": lambda p: p in auth[1],
        "datetime_now": datetime.utcnow(),
        "py": py,
        "rbac": {
            "role_keys": list(rbac.roles.keys()),
            "roles": rbac.roles
        },
        "service_config": current_config
    }
