from typing import List, Generic, TypeVar, Tuple

from pydantic import BaseModel

T = TypeVar("T")


class Page(BaseModel):
    index: int = 0
    offset: int = 50


class PageStats(BaseModel):
    total_items: int = 0
    total_pages: int = 0


class PageOut(BaseModel, Generic[T]):
    page: Page
    stats: PageStats
    data: List[T]

    @property
    def paginator(self) -> List[Tuple[str, int]]:
        if self.stats.total_pages < 8:
            return [(str(x + 1), x) for x in range(self.stats.total_pages)]

        left = [(str(x + 1), x) for x in range(3)]
        right = [(str(x + 1), x) for x in range(self.stats.total_pages - 4, self.stats.total_pages - 1)]
        middle = ("...", (self.stats.total_pages - 1) // 2)

        return [*left, middle, *right]

    @property
    def need_pagination(self) -> bool:
        return self.stats.total_pages > 1
