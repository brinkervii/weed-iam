from typing import Annotated

import httpx
from fastapi import APIRouter, Form
from fastapi import HTTPException, Request
from fastapi.responses import HTMLResponse

from weed_iam import oauth
from weed_iam.app_common import require_permissions, \
    AuthPermissions, apps_repository
from weed_iam.app_common import template_context, templates
from weed_iam.rbac import Permission
from weed_iam.util import random_fun

router = APIRouter(tags=["oauth_test"])


@router.get("/oauth2/init", response_class=HTMLResponse)
async def select_weed_app_to_oauth_with(request: Request, auth: AuthPermissions):
    require_permissions(auth, [Permission.oauth2_test_authorization, Permission.list_apps, Permission.read_app])

    return templates.TemplateResponse(
        "oauth/select_app.html",
        template_context(
            request=request,
            title="Weed IAM - OAuth Authorization",
            auth=auth,
            data={
                "apps": await apps_repository.list_apps()
            }
        )
    )


@router.get("/oauth2/init/callback", response_class=HTMLResponse)
async def oauth2_callback(request: Request, auth: AuthPermissions, code: str = None):
    require_permissions(auth, [Permission.oauth2_test_authorization])

    return templates.TemplateResponse(
        "oauth/callback_page.html",
        template_context(
            request=request,
            title="Weed IAM - OAuth Authorization",
            auth=auth,
            data={
                "oauth_code": code
            }
        )
    )


@router.post("/oauth2/init/callback", response_class=HTMLResponse)
async def claim_oauth_code(request: Request, auth: AuthPermissions, oauth_code: Annotated[str, Form()]):
    require_permissions(auth, [Permission.oauth2_test_authorization])

    code_handle = oauth.codes_in_flight.get(oauth_code, None)
    if code_handle is None:
        raise HTTPException(status_code=404, detail="Code is invalid or expired")

    weed_app = await apps_repository.find_app(code_handle.request.client_id)
    if not weed_app:
        raise HTTPException(status_code=418, detail="Lost code app reference?")

    # Assume test OAuth provider is on same host
    base_url = str(request.base_url).rstrip("/")
    token_url = f"{base_url}/oauth2/token"

    async with httpx.AsyncClient() as client:
        response: httpx.Response = await client.post(
            token_url,
            json={
                "grant_type": "code",
                "client_id": weed_app["app_id"],
                "client_secret": weed_app["app_secret"],
                "code": oauth_code
            }
        )

        try:
            response.raise_for_status()

        except httpx.HTTPError as e:
            raise HTTPException(status_code=400, detail=f"Failed to claim code: {e}\n\n{response.text}")

    response_data = response.json()

    return templates.TemplateResponse(
        "oauth/show_code_claim_result.html",
        template_context(
            request=request,
            title="Weed IAM - OAuth Authorization",
            auth=auth,
            data={
                "token_response": response_data
            }
        )
    )


@router.get("/oauth2/init/{app_id}", response_class=HTMLResponse)
async def set_oauth_params(request: Request, auth: AuthPermissions, app_id: str):
    require_permissions(auth, [Permission.oauth2_test_authorization, Permission.list_apps, Permission.read_app])

    if weed_app := await apps_repository.find_app(app_id):
        return templates.TemplateResponse(
            "oauth/init_authorization.html",
            template_context(
                request=request,
                title="Weed IAM - OAuth Authorization",
                auth=auth,
                data={
                    "response_types": oauth.grant_types,
                    "app": weed_app,
                    "oauth_scopes": oauth.all_oauth_scopes,
                    "state": random_fun.random_bytes_hex_encoded(16)
                }
            )
        )

    raise HTTPException(status_code=404, detail="Not Found")
