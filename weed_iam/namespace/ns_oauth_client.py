import logging
import random
import re
import time
from html import escape
from typing import Optional, Annotated

from fastapi import APIRouter, Form, HTTPException, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from jose import jwt
from pydantic import BaseModel

from weed_iam import oauth
from weed_iam.app_common import require_permissions, \
    AuthPermissions, apps_repository, user_repository
from weed_iam.app_common import template_context, templates
from weed_iam.rbac import Permission
from weed_iam.service_config import current_config

log = logging.getLogger(__name__)
router = APIRouter(tags=["oauth_client"])


class OAuth2TokenRequest(BaseModel):
    grant_type: str
    client_id: str
    client_secret: str
    code: str


class OAuth2TokenResponse(BaseModel):
    token_type: str
    refresh_token: Optional[str] = None
    access_token: Optional[str] = None


@router.get("/oauth2/authorize", response_class=HTMLResponse)
async def oauth2_authorization_page(
        request: Request,
        auth: AuthPermissions,
        response_type: str,
        client_id: str,
        redirect_uri: str,
        scope: str,
        state: str
):
    oauth.clean_codes_in_flight()

    require_permissions(auth, [Permission.oauth2_authorise])

    request_scope_list = list(filter(None, map(str.strip, re.split(r"\s+", scope))))
    for scope in request_scope_list:
        if scope not in oauth.all_oauth_scope_strings:
            raise HTTPException(status_code=400, detail=f"Invalid OAuth scope {scope}")

    authorization_request = oauth.AuthorizationRequestModel(
        response_type=response_type,
        client_id=client_id,
        redirect_uri=redirect_uri,
        scope=list(map(lambda s: oauth.OAuthScope(s), request_scope_list)),
        state=escape(state)  # State is echoed back so it needs to be clean
    )

    log.info(authorization_request.model_dump_json(indent=2))

    if authorization_request.response_type not in oauth.grant_types:
        raise HTTPException(status_code=400, detail="Invalid response type")

    if authorization_request.response_type != "code":
        raise HTTPException(status_code=400, detail="Only 'code' grants are allowed for this endpoint")

    weed_app = await apps_repository.find_app(authorization_request.client_id)
    if not weed_app:
        log.error(
            f"{oauth2_authorization_page} :: "
            f"Attempted to access unknown client id {authorization_request.client_id}"
        )
        raise HTTPException(status_code=404, detail="Application does not exist")

    redirect_uri_is_valid = authorization_request.redirect_uri in weed_app["callback_urls"]
    if not redirect_uri_is_valid:
        log.error(
            f"{oauth2_authorization_page} :: "
            f"Attempted to authorize with invalid redirect uri {authorization_request.redirect_uri}"
        )
        raise HTTPException(status_code=400, detail="Invalid callback URI")

    user = await user_repository.find_user(auth[0]["sub"])
    if not user:
        raise HTTPException(status_code=403, detail="No user")

    if user["user_id"] != auth[2]["user_id"]:
        raise HTTPException(status_code=401, detail="???")

    code = oauth.fly_code(authorization_request, {
        "sec": oauth.OAUTH_CODE_SECRET,
        "rng": random.randint(0, 9999),
        "sub": user["user_id"],
        "iat": int(time.time()),
        "exp": int(time.time()) + 1800
    })

    return templates.TemplateResponse(
        "oauth/authorize.html",
        template_context(
            request=request,
            title=f"Weed IAM - {weed_app['name']}",
            auth=auth,
            data={
                "user": user,
                "app": weed_app,
                "auth_request": authorization_request,
                "oauth_code": code,
                "oauth_state": authorization_request.state,
                "OAuthScope": oauth.OAuthScope,
                "hide_authentication_buttons": True
            }
        )
    )


@router.post("/oauth2/authorize")
async def oauth2_grant_authorization(
        auth: AuthPermissions,
        oauth_code: Annotated[str, Form()],
        oauth_state: Annotated[str, Form()],
        redirect_url: Annotated[str, Form()]
):
    oauth.clean_codes_in_flight()

    require_permissions(auth, [Permission.oauth2_authorise])

    if oauth_code not in oauth.codes_in_flight:
        raise HTTPException(status_code=401, detail="Invalid code")

    claims = oauth.unmake_code(oauth_code)
    if not claims:
        raise HTTPException(status_code=401, detail="Invalid code")

    flight_handle = oauth.codes_in_flight[oauth_code]

    user = await user_repository.find_user(claims["sub"])
    if not user:
        raise HTTPException(status_code=401, detail="Invalid code")

    weed_app = await apps_repository.find_app(flight_handle.request.client_id)
    if not weed_app:
        raise HTTPException(status_code=401, detail="Invalid code")

    await user_repository.append_application_grant(user["user_id"], weed_app["app_id"])

    rx = [
        redirect_url.strip("/"),
        f"?code={oauth_code}",
        f"&state={oauth_state}"
    ]

    return RedirectResponse("".join(rx), status_code=302)


@router.post("/oauth2/token", response_model=OAuth2TokenResponse)
async def oauth2_code_claim(token_request: OAuth2TokenRequest):
    oauth.clean_codes_in_flight()

    if token_request.grant_type not in oauth.grant_types:
        raise HTTPException(status_code=400, detail="Invalid grant type")

    weed_app = await apps_repository.find_app(token_request.client_id)
    if weed_app is None:
        raise HTTPException(status_code=404, detail="App does not exist")

    if token_request.client_secret != weed_app["app_secret"]:
        raise HTTPException(status_code=401, detail="Not Authorized to access resource")

    if token_request.grant_type == "code":
        code_claim: oauth.CodeClaims = oauth.unmake_code(token_request.code, remove_from_flight=True)
        if code_claim is None:
            raise HTTPException(status_code=400, detail="Invalid code")

        user = await user_repository.find_user(code_claim["sub"])

    elif token_request.grant_type == "refresh_token":
        refresh_decoded = jwt.decode(token_request.code, current_config.jwt_secret, [current_config.jwt_algorithm])

        if int(time.time()) > refresh_decoded["exp"]:
            raise HTTPException(status_code=400, detail="Refresh token has expired")

        user = await user_repository.find_user(refresh_decoded["sub"])

    else:
        raise HTTPException(status_code=400, detail="Invalid grant type")

    if not user:
        raise HTTPException(status_code=401, detail="Not Authorized to access resource")

    if weed_app["app_id"] not in user["app_grants"]:
        raise HTTPException(status_code=401, detail="User did not grant access for this app")

    refresh_token = jwt.encode(
        claims={
            "sub": user["user_id"],
            "iat": int(time.time()),
            "exp": int(time.time()) + 86400,  # Valid for 1 day
            "iss": current_config.jwt_issuer,
            "typ": "refresh_token"
        },
        key=current_config.jwt_secret,
        algorithm=current_config.jwt_algorithm
    )

    access_token = jwt.encode(
        claims={
            "sub": user["user_id"],
            "iat": int(time.time()),
            "exp": int(time.time()) + 1800,  # Valid for 30 minutes
            "iss": current_config.jwt_issuer,
            "typ": "access_token"
        },
        key=current_config.jwt_secret,
        algorithm=current_config.jwt_algorithm
    )

    return {
        "token_type": "bearer",
        "refresh_token": refresh_token,
        "access_token": access_token
    }
