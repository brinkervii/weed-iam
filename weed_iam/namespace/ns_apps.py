import uuid
from typing import Annotated

from fastapi import APIRouter
from fastapi import HTTPException, Request, Form
from fastapi.responses import HTMLResponse, RedirectResponse

from weed_iam.app_common import AuthPermissions, require_permissions, templates, template_context, apps_repository
from weed_iam.rbac import Permission

router = APIRouter(tags=["admin_pages_apps"])


@router.get("/apps", response_class=HTMLResponse)
async def list_apps(auth: AuthPermissions, request: Request):
    require_permissions(auth, [Permission.list_apps])

    return templates.TemplateResponse(
        "apps/apps.html",
        template_context(
            request=request,
            title="Weed IAM - Apps",
            auth=auth,
            data={
                "apps": await apps_repository.list_apps(),
                "truncate_secret": lambda s: s[:30] + "..."
            }
        )
    )


@router.get("/apps/create", response_class=HTMLResponse)
async def app_create(request: Request, auth: AuthPermissions):
    require_permissions(auth, [Permission.create_app])

    return templates.TemplateResponse(
        "apps/create_app.html",
        template_context(
            request=request,
            title="Weed IAM - New App",
            auth=auth
        )
    )


@router.post("/apps/create", response_class=HTMLResponse)
async def app_create(
        auth: AuthPermissions,
        request: Request,
        app_name: Annotated[str, Form()],
        callback_urls: Annotated[str, Form()]
):
    require_permissions(auth, [Permission.create_app])

    callback_list = list(filter(None, map(str.strip, callback_urls.split("\n"))))

    if await apps_repository.create_app(app_name, callback_list):
        return RedirectResponse("/apps", status_code=302)

    return templates.TemplateResponse(
        "apps/create_app.html",
        template_context(
            request=request,
            title="Weed IAM - New App"
        )
    )


@router.get("/apps/{app_id}/secret", response_class=HTMLResponse)
async def get_app_secret(request: Request, auth: AuthPermissions, app_id: str):
    require_permissions(auth, [Permission.list_apps, Permission.read_app, Permission.copy_app_secret])

    if weed_app := await apps_repository.find_app(app_id):
        return templates.TemplateResponse(
            "apps/copy_secret_modal.html",
            template_context(
                request=request,
                title="Weed IAM - New App",
                data={
                    "modal_id": str(uuid.uuid4()),
                    "app": weed_app
                }
            )
        )

    raise HTTPException(status_code=404)


@router.get("/apps/{app_id}/reset_secret", response_class=HTMLResponse)
async def reset_app_secret(request: Request, auth: AuthPermissions, app_id: str):
    require_permissions(auth, [Permission.list_apps, Permission.read_app, Permission.update_app_secret])

    if weed_app := await apps_repository.find_app(app_id):
        return templates.TemplateResponse(
            "apps/reset_secret.html",
            template_context(
                request=request,
                title="Weed IAM - New App",
                data={
                    "app": weed_app
                }
            )
        )

    raise HTTPException(status_code=404)


@router.post("/apps/{app_id}/reset_secret", response_class=HTMLResponse)
async def reset_app_secret(request: Request, auth: AuthPermissions, app_id: str):
    require_permissions(auth, [Permission.list_apps, Permission.read_app, Permission.update_app_secret])

    if await apps_repository.reset_secret(app_id):
        return templates.TemplateResponse(
            "partials/apps_list.html",
            template_context(
                request=request,
                title="Weed IAM - Apps",
                auth=auth,
                data={
                    "app_id": app_id,
                    "app_operation": "reset_secret",
                    "apps": await apps_repository.list_apps(),
                    "truncate_secret": lambda s: s[:30] + "..."
                }
            )
        )

    raise HTTPException(status_code=400)


@router.get("/apps/{app_id}/edit", response_class=HTMLResponse)
async def edit_app_card(request: Request, auth: AuthPermissions, app_id: str):
    require_permissions(auth, [Permission.list_apps, Permission.read_app, Permission.update_app])

    if weed_app := await apps_repository.find_app(app_id):
        return templates.TemplateResponse(
            "apps/edit_app.html",
            template_context(
                request=request,
                title="Weed IAM - New App",
                data={
                    "app": weed_app
                }
            )
        )

    raise HTTPException(status_code=404)


@router.post("/apps/{app_id}/edit", response_class=HTMLResponse)
async def update_app(
        request: Request,
        auth: AuthPermissions,
        app_id: str,
        app_name: Annotated[str, Form()],
        callback_urls: Annotated[str, Form()]
):
    require_permissions(auth, [Permission.list_apps, Permission.read_app, Permission.update_app])

    callback_list = list(filter(None, map(str.strip, callback_urls.split("\n"))))

    ok = await apps_repository.update_app(
        app_id,
        app_name,
        callback_list
    )

    if ok:
        return templates.TemplateResponse(
            "partials/apps_list.html",
            template_context(
                request=request,
                title="Weed IAM - Apps",
                auth=auth,
                data={
                    "app_id": app_id,
                    "app_operation": "update",
                    "apps": await apps_repository.list_apps(),
                    "truncate_secret": lambda s: s[:30] + "...",
                }
            )
        )

    raise HTTPException(status_code=400)


@router.get("/apps/{app_id}/delete", response_class=HTMLResponse)
async def delete_app_card(request: Request, auth: AuthPermissions, app_id: str):
    require_permissions(auth, [Permission.list_apps, Permission.read_app, Permission.delete_app])

    if weed_app := await apps_repository.find_app(app_id):
        return templates.TemplateResponse(
            "apps/delete_app.html",
            template_context(
                request=request,
                title="Weed IAM - New App",
                data={
                    "app": weed_app
                }
            )
        )

    raise HTTPException(status_code=404)


@router.post("/apps/{app_id}/delete", response_class=HTMLResponse)
async def delete_app(request: Request, auth: AuthPermissions, app_id: str):
    require_permissions(auth, [Permission.list_apps, Permission.read_app, Permission.delete_app])

    if await apps_repository.delete_app(app_id):
        return templates.TemplateResponse(
            "partials/apps_list.html",
            template_context(
                request=request,
                title="Weed IAM - Apps",
                auth=auth,
                data={
                    "app_id": app_id,
                    "app_operation": "delete",
                    "apps": await apps_repository.list_apps(),
                    "truncate_secret": lambda s: s[:30] + "...",
                }
            )
        )

    raise HTTPException(status_code=400)
