from fastapi import Request, APIRouter
from fastapi.responses import HTMLResponse

from weed_iam import rbac
from weed_iam.app_common import template_context, templates, AuthPermissions, require_permissions, user_repository
from weed_iam.rbac import Permission

router = APIRouter(tags=["admin_pages"])


@router.get("/dashboard", response_class=HTMLResponse)
async def app_index(auth: AuthPermissions, request: Request):
    require_permissions(auth, [Permission.use_app])

    return templates.TemplateResponse(
        "dashboard.html",
        template_context(
            request=request,
            auth=auth,
            data={
                "performance": {
                    "total_users": await user_repository.count_users(),
                    "total_roles": await user_repository.count_roles(),
                    "total_permissions": await user_repository.count_permissions()
                }
            }
        )
    )


@router.get("/roles", response_class=HTMLResponse)
async def list_roles(request: Request, auth: AuthPermissions):
    require_permissions(auth, [Permission.list_roles])

    return templates.TemplateResponse(
        "roles.html",
        template_context(
            request=request,
            title="Weed IAM - Roles",
            auth=auth,
            data={
                "roles": rbac.roles
            }
        )
    )


@router.get("/permissions", response_class=HTMLResponse)
async def list_permissions(request: Request, auth: AuthPermissions):
    require_permissions(auth, [Permission.list_roles])

    return templates.TemplateResponse(
        "permissions.html",
        template_context(
            request=request,
            title="Weed IAM - Permissions",
            auth=auth,
            data={
                "permissions": rbac.all_permissions
            }
        )
    )


@router.get("/settings", response_class=HTMLResponse)
async def iam_settings_page(request: Request, auth: AuthPermissions):
    require_permissions(auth, [Permission.list_roles])

    return templates.TemplateResponse(
        "settings/settings.html",
        template_context(
            request=request,
            title="Weed IAM - Settings",
            auth=auth,
        )
    )
