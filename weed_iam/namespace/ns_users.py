from fastapi import APIRouter, Request

from weed_iam.app_common import require_permissions, AuthPermissions, templates, template_context, user_repository
from weed_iam.pagination import Page
from weed_iam.rbac import Permission

router = APIRouter(tags=["admin_pages_users"])


@router.get("/users")
async def users_page(request: Request, auth: AuthPermissions, page_index: int = 0, page_offset: int = 50):
    require_permissions(auth, [Permission.list_users])

    page = await user_repository.list_users(Page(index=page_index, offset=page_offset))

    return templates.TemplateResponse(
        "users/users.html",
        template_context(
            request=request,
            title="Weed IAM - Users",
            auth=auth,
            data={
                "page": page
            }
        )
    )
