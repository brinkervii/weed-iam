from typing import Annotated

from fastapi import APIRouter, HTTPException
from fastapi import Request, Form
from fastapi.responses import HTMLResponse, RedirectResponse

from weed_iam.app_common import templates, template_context, IsLoggedIn, user_repository, unmake_redirect_string
from weed_iam.service_config import current_config
from weed_iam.util.alert_builder import AlertBuilder
from weed_iam.util.validation import validate_email_address

router = APIRouter(tags=["iam_authentication"])


@router.get("/login", response_class=HTMLResponse)
async def login(request: Request, logged_in: IsLoggedIn, redirect: str = ""):
    if logged_in:
        return RedirectResponse("/dashboard", 302)

    return templates.TemplateResponse(
        "login.html",
        template_context(
            request=request,
            data={
                "redirect": redirect
            }
        )
    )


@router.post("/login", response_class=HTMLResponse)
async def login(
        request: Request,
        logged_in: IsLoggedIn,
        email: Annotated[str, Form()] = "",
        password: Annotated[str, Form()] = "",
        redirect: Annotated[str, Form()] = ""
):
    if logged_in:
        return RedirectResponse("/dashboard", 302)

    token = None
    alerts = AlertBuilder()

    email_validation = validate_email_address(email)
    if email_validation.ok:
        token = await user_repository.login_make_cookie(email, password)

    else:
        alerts.danger(str(email_validation))

    if token is None:
        alerts.danger("Could not long in. Make sure your email and password are correct!")

        return templates.TemplateResponse(
            "login.html",
            template_context(
                request=request,
                data={
                    "alerts": alerts.export(),
                    "form": {
                        "email": email
                    },
                    "redirect": redirect
                }
            )
        )

    else:
        if redirect:
            redirect = unmake_redirect_string(redirect)

            response = RedirectResponse(redirect, status_code=302)

        else:
            response = RedirectResponse("/dashboard", status_code=302)

        response.set_cookie(key=current_config.session_cookie_name, value=token)

        return response


@router.get("/logout", response_class=HTMLResponse)
async def logout():
    response = RedirectResponse("/", status_code=302)
    response.delete_cookie(current_config.session_cookie_name)

    return response  # TODO: Display logout page


async def _register_route_common():
    alerts = AlertBuilder()

    root_users = await user_repository.find_root_users()
    setup_mode = len(root_users) == 0

    if setup_mode:
        alerts.warning(
            "This instance of Weed IAM is in setup mode."
            "The user that you create now will receive 'root' role."
        )

    if not setup_mode and not current_config.self_sign_up_enabled:
        raise HTTPException(status_code=403, detail="Self sign-up is not enabled. Please contact an administrator.")

    return alerts, setup_mode


@router.get("/register", response_class=HTMLResponse)
async def register(request: Request, logged_in: IsLoggedIn):
    if logged_in:
        return RedirectResponse("/dashboard", 302)

    alerts, _setup_mode = await _register_route_common()

    return templates.TemplateResponse(
        "register.html",
        template_context(
            request=request,
            data={
                "alerts": alerts.export()
            }
        )
    )


@router.post("/register", response_class=HTMLResponse)
async def create_account(
        request: Request,
        logged_in: IsLoggedIn,
        email: Annotated[str, Form()] = "",
        password: Annotated[str, Form()] = "",
        repeat_password: Annotated[str, Form()] = ""
):
    if logged_in:
        raise HTTPException(status_code=418, detail="Stop playing around!")

    alerts, setup_mode = await  _register_route_common()

    def fail():
        return templates.TemplateResponse(
            "register.html",
            template_context(
                request=request,
                data={
                    "alerts": alerts.export(),
                    "form": {
                        "email": email
                    }
                }
            )
        )

    email_validation = validate_email_address(email)
    if not email_validation.ok:
        alerts.danger(str(email_validation))
        return fail()

    existing_user = await user_repository.find_user(email)
    if existing_user is not None:
        alerts.danger("An account has already been registered using this email address.")
        return fail()

    if password != repeat_password:
        alerts.danger("The repeated password did not match.")
        return fail()

    if setup_mode:
        roles = ["root", "user"]

    else:
        roles = ["user"]

    new_user = await user_repository.create_user(email, password, roles)

    if new_user is not None:
        response = RedirectResponse("/register/success", status_code=302)
        response.delete_cookie(current_config.session_cookie_name)  # Just in case

        return response

    else:
        alerts.danger(
            "Failed to create user. Please try again later. "
            "If the issue persists, please contact an administrator"
        )


@router.get("/register/success", response_class=HTMLResponse)
async def register(request: Request):
    return templates.TemplateResponse(
        "register-success.html",
        template_context(request)
    )
