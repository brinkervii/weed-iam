import base64
import os
import random

import string


def random_bytes_base64_encoded(num_bytes: int) -> str:
    random_bytes = os.urandom(num_bytes)
    encoded_bytes = base64.b64encode(random_bytes).decode('utf-8')

    return encoded_bytes


def random_bytes_hex_encoded(num_bytes: int) -> str:
    random_bytes = os.urandom(num_bytes)
    hex_encoded = random_bytes.hex()

    return hex_encoded


def make_random_alphanumeric_string(length: int) -> str:
    alphanumeric_chars = string.ascii_letters + string.digits
    return "".join(random.choice(alphanumeric_chars) for _ in range(length))
