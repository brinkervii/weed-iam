import logging
from dataclasses import dataclass
from email.utils import parseaddr
from typing import List, Optional

from dns.resolver import resolve as dns_query

log = logging.getLogger(__name__)


@dataclass
class EmailValidationResult:
    ok: bool
    reasons: Optional[List[str]] = None

    def __str__(self):
        msg = ["The email address provided is invalid for the following reasons:\n"]
        msg.extend([f"- {s}" for s in self.reasons])

        return "\n".join(msg)


def validate_email_address(email_address: str):
    has_at = "@" in email_address

    parsed_ok, query_ok = False, False

    if has_at:
        parsed = parseaddr(email_address)
        parsed_ok = len(list(filter(None, map(str.strip, parsed)))) > 0

        if parsed_ok:
            domain = email_address.rsplit("@", 1)[-1]

            if email_address.lower().endswith(".local"):
                query_ok = True

            else:
                try:
                    _ = dns_query(domain)
                    query_ok = True

                except Exception as e:
                    log.warning(e)
                    query_ok = False

    reasons = []

    if not has_at:
        reasons.append("Email address must contain an @ symbol.")

    if not parsed_ok:
        reasons.append("Could not parse email address.")

    if not query_ok:
        reasons.append("Email address has an invalid domain")

    return EmailValidationResult(has_at and parsed_ok and query_ok, reasons)
