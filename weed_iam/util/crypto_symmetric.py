from cryptography.fernet import Fernet


def generate_key():
    """
    Generates a new encryption key.
    Returns:
        bytes: The encryption key.
    """
    return Fernet.generate_key()


def encrypt(plaintext, key):
    """
    Encrypts the plaintext using the given encryption key.
    Args:
        plaintext (bytes): The plaintext to encrypt.
        key (bytes): The encryption key.
    Returns:
        bytes: The encrypted ciphertext.
    """
    cipher = Fernet(key)
    ciphertext = cipher.encrypt(plaintext)
    return ciphertext


def decrypt(ciphertext, key):
    """
    Decrypts the ciphertext using the given encryption key.
    Args:
        ciphertext (bytes): The ciphertext to decrypt.
        key (bytes): The encryption key.
    Returns:
        bytes: The decrypted plaintext.
    """
    cipher = Fernet(key)
    plaintext = cipher.decrypt(ciphertext)
    return plaintext
