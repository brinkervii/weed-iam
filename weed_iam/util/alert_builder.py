from __future__ import annotations

from dataclasses import dataclass, asdict
from typing import List, TypedDict

import markdown


class UnstructuredAlert(TypedDict):
    severity: str
    message: str


@dataclass
class Alert:
    severity: str
    message: str


class AlertBuilder:
    alerts: List[Alert]

    def __init__(self):
        self.alerts = []

    def export(self) -> List[UnstructuredAlert]:
        unstructured: List[UnstructuredAlert] = [asdict(x) for x in self.alerts]

        for alert in unstructured:
            alert["message"] = markdown.markdown(
                alert["message"],
                extensions=(
                    "markdown.extensions.smarty",
                    "mdx_truly_sane_lists"
                )
            )

        return unstructured

    def primary(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("primary", " ".join(message)))
        return self

    def secondary(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("secondary", " ".join(message)))
        return self

    def success(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("success", " ".join(message)))
        return self

    def danger(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("danger", " ".join(message)))
        return self

    def warning(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("warning", " ".join(message)))
        return self

    def info(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("info", " ".join(message)))
        return self

    def light(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("light", " ".join(message)))
        return self

    def dark(self, *message: str) -> AlertBuilder:
        self.alerts.append(Alert("dark", " ".join(message)))
        return self
