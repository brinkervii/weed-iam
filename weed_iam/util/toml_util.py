from pathlib import Path
from typing import Dict

import toml


def try_load_toml(path: Path) -> Dict:
    if path.exists():
        with path.open("r") as fp:
            return toml.load(fp)

    else:
        return {}


def save_toml(path: Path, o):
    path.parent.mkdir(exist_ok=True, parents=True)

    with path.open("w+") as fp:
        toml.dump(o, fp)
