import logging

from fastapi import FastAPI, Depends, HTTPException, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.security import OAuth2PasswordBearer
from fastapi.staticfiles import StaticFiles
from jose import jwt

from weed_iam.app_common import IsLoggedIn, template_context, templates, user_repository
from weed_iam.service_config import current_config

app = FastAPI()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")

app.mount("/static", StaticFiles(directory="static"), name="static")

log = logging.getLogger(__name__)


@app.get("/", response_class=HTMLResponse)
async def index(request: Request, is_logged_in: IsLoggedIn):
    if is_logged_in:
        return RedirectResponse("/dashboard", 302)

    return templates.TemplateResponse("index.html", template_context(request, is_logged_in=is_logged_in))


@app.get("/poll")
async def poll():
    return {}


@app.get("/users/me")
async def read_user_me(token: str = Depends(oauth2_scheme)):
    claims = jwt.decode(token, current_config.jwt_secret, algorithms=[current_config.jwt_algorithm])

    return await user_repository.find_user(claims["sub"])


def register_routers():
    from weed_iam.namespace import ns_admin
    from weed_iam.namespace import ns_apps
    from weed_iam.namespace import ns_authentication
    from weed_iam.namespace import ns_oauth_admin
    from weed_iam.namespace import ns_oauth_client
    from weed_iam.namespace import ns_users

    routers = [
        ns_admin.router,
        ns_apps.router,
        ns_authentication.router,
        ns_oauth_admin.router,
        ns_oauth_client.router,
        ns_users.router
    ]

    for router in routers:
        app.include_router(router)


register_routers()

H400_STATUS_DEFAULT = ("Oops! Something went wrong.", "We're sorry, but the page you're looking for cannot be found.")

H400_STATUS = {
    400: ("Bad Request", "Oops! It seems like the request sent by the client is not valid."),
    401: ("Unauthorized", "Sorry, but you don't have the necessary credentials to access this resource."),
    402: (
        "Payment Required",
        "To continue, please make the required payment. You can find the details on our payment page."),
    403: ("Forbidden", "We're sorry, but you don't have permission to access this content."),
    404: H400_STATUS_DEFAULT,
    405: ("Method Not Allowed", "Please ensure that you are using the appropriate HTTP method for this endpoint."),
    406: ("Not Acceptable", "The requested resource cannot be served with the given content type or language."),
    407: ("Proxy Authentication Required", "Authentication with the proxy server is required."),
    408: (
        "Request Timeout", "The server did not receive a complete request from the client within the specified time."),
    409: ("Conflict", "The requested operation conflicts with the current state of the resource."),
    410: ("Gone", "The requested resource is no longer available and has been permanently removed."),
    411: ("Length Required", "The 'Content-Length' header is missing or invalid."),
    412: ("Precondition Failed", "The server does not meet the preconditions specified in the request."),
    413: ("Payload Too Large", "The request is larger than the server is willing or able to process."),
    414: ("URI Too Long", "The requested URI is longer than the server is willing to interpret."),
    415: ("Unsupported Media Type", "The server does not support the media type or format of the requested resource."),
    416: ("Range Not Satisfiable", "The requested range is not satisfiable."),
    417: ("Expectation Failed", "The server cannot meet the requirements of the 'Expect' header field."),
    418: ("I'm a Teapot", "I'm a teapot. This HTTP status is a joke."),
    421: ("Misdirected Request", "The request was directed at a server that is not able to produce a response."),
    422: ("Unprocessable Entity", "The request was well-formed but unable to be followed due to semantic errors."),
    423: ("Locked", "The resource that is being accessed is locked."),
    424: ("Failed Dependency", "The request failed due to a failure of a previous request."),
    426: ("Upgrade Required", "The client should switch to a different protocol."),
    428: ("Precondition Required", "The origin server requires the request to be conditional."),
    429: ("Too Many Requests", "You have exceeded the rate limit for this resource. Please try again later."),
    431: ("Request Header Fields Too Large",
          "The server is unwilling to process the request because its header fields are too large."),
    451: ("Unavailable For Legal Reasons", "The requested resource is unavailable due to legal reasons."),
}

for status_code in H400_STATUS.keys():
    @app.exception_handler(status_code)
    def http_status_handler(request: Request, exc: HTTPException):
        message_short, message_long = H400_STATUS.get(exc.status_code, H400_STATUS_DEFAULT)

        return templates.TemplateResponse(
            "error.html",
            template_context(
                request=request,
                title="Weed IAM",
                data={
                    "status_code": exc.status_code,
                    "exc": exc,
                    "message_short": message_short,
                    "message_long": message_long,
                    "hide_authentication_buttons": True
                }
            )
        )

log.info("Initialized app module")
