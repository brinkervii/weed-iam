import os
from dataclasses import dataclass, field
from pathlib import Path
from typing import TypeVar, Callable

from dotenv import load_dotenv

from weed_iam.util import crypto_symmetric

T = TypeVar("T")

Parse = Callable[[str], T]


def parse_noop(x: T) -> T:
    return x


def parse_bool(x: str) -> bool:
    x = x.strip().lower()

    if x.isnumeric():
        return int(x) > 0

    return x == "true"


def clean_url_definition(url: str) -> str:
    return url.strip().rstrip("/")


def from_env(key: str, default_value: str = None, parse: Parse = parse_noop) -> T:
    def load():
        v = os.environ.get(key, None)

        if v is None:
            if default_value is not None:
                return parse(default_value)

            raise RuntimeError(f"Missing environment variable {key}")

        return parse(v)

    return load


def db_path() -> Path:
    return Path(os.environ.get("DB_DIR", "db")).resolve()


def static_content_path() -> Path:
    return Path(os.environ.get("STATIC_DIR", "static")).resolve()


def templates_path() -> Path:
    return Path(os.environ.get("TEMPLATES_DIR", "templates")).resolve()


@dataclass
class ServiceConfiguration:
    public_base_url: str = field(default_factory=from_env("PUBLIC_BASE_URL", parse=clean_url_definition))

    jwt_secret: str = field(default_factory=from_env("JWT_SECRET"))
    jwt_algorithm: str = field(default_factory=from_env("JWT_ALGORITHM", "HS256"))
    session_cookie_name: str = field(default_factory=from_env("SESSION_COOKIE_NAME", "WeedIAM.Session"))

    self_sign_up_enabled: str = field(default_factory=from_env("ENABLE_SELF_SIGN_UP", "False", parse=parse_bool))

    user_db_path: Path = field(default_factory=lambda: db_path() / "users.toml")
    apps_db_path: Path = field(default_factory=lambda: db_path() / "apps.toml")

    static_content_path: Path = field(default_factory=lambda: static_content_path())
    templates_path: Path = field(default_factory=lambda: templates_path())

    ephemeral_symmetric_cryptography_key: bytes = field(default_factory=crypto_symmetric.generate_key)

    @property
    def jwt_issuer(self):
        return self.public_base_url


load_dotenv()
current_config = ServiceConfiguration()
