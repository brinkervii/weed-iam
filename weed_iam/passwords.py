from dataclasses import dataclass

from passlib.hash import pbkdf2_sha256, pbkdf2_sha512

hash_algorithms = {
    "pbkdf2_sha512": pbkdf2_sha512,
    "pbkdf2_sha256": pbkdf2_sha256,
}

default_hash_algorithm = list(hash_algorithms.keys())[0]


def is_valid_hash_algorithm(hash_algorithm: str) -> bool:
    return hash_algorithm in hash_algorithms


@dataclass
class HashPasswordResult:
    hash: str
    hash_algorithm: str


def hash_password(password: str, hash_algorithm: str = default_hash_algorithm) -> HashPasswordResult:
    return HashPasswordResult(
        hash=hash_algorithms[hash_algorithm].hash(password),
        hash_algorithm=hash_algorithm
    )


def verify_password(
        plaintext_password: str,
        hashed_password: str,
        hash_algorithm: str = default_hash_algorithm
) -> bool:
    return hash_algorithms[hash_algorithm].verify(plaintext_password, hashed_password)
