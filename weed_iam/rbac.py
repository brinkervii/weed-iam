from collections import defaultdict
from dataclasses import dataclass, field
from enum import Enum
from typing import List, Dict


class Permission(Enum):
    use_app = 0

    list_apps = 10
    create_app = 11
    read_app = 12
    update_app = 13
    delete_app = 14
    copy_app_secret = 15
    update_app_secret = 16

    list_roles = 20
    list_permissions = 21

    oauth2_test_authorization = 30
    oauth2_authorise = 31

    list_users = 41
    create_user = 42
    read_user = 43
    update_user = 44
    delete_user = 45

    admin = 600
    root = 666


root_permissions = [Permission.root]
all_permissions = [e for e in Permission if e not in root_permissions]


@dataclass
class Role:
    permissions: List[Permission] = field(default_factory=list)


roles: Dict[str, Role] = defaultdict(lambda: Role())

roles["user"] = Role(permissions=[
    Permission.use_app,
    Permission.oauth2_authorise
])

roles["admin"] = Role(permissions=[*all_permissions])

roles["root"] = Role(permissions=[*root_permissions, *all_permissions])
