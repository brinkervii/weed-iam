import asyncio
import os
import signal
from typing import Any

import mdx_truly_sane_lists
import uvloop
from hypercorn.asyncio import serve
from hypercorn.config import Config
from hypercorn.typing import ASGIFramework

from weed_iam.app import app

# Lazy hack to get PyInstaller to import it
print(f"Have MDX Truly sane lists: {mdx_truly_sane_lists is not None}")

asgi_app: ASGIFramework = app

host = os.environ.get("HOST", "127.0.0.1")
port = int(os.environ.get("PORT", "8000"))
log_level = os.environ.get("LOG_LEVEL", "INFO")

config = Config()
config.bind = [f"{host}:{port}"]
config.loglevel = log_level

uvloop.install()

shutdown_event = asyncio.Event()


def _signal_handler(*_: Any) -> None:
    shutdown_event.set()


loop = asyncio.get_event_loop()
loop.add_signal_handler(signal.SIGTERM, _signal_handler)
loop.add_signal_handler(signal.SIGINT, _signal_handler)


async def shutdown_trigger():
    await shutdown_event.wait()


loop.run_until_complete(
    serve(
        asgi_app,
        config,
        shutdown_trigger=shutdown_trigger
    )
)
