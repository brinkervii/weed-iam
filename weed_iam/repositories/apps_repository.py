import base64
import sys
import uuid
from pathlib import Path
from typing import Protocol, TypedDict, Dict, List, Optional

import magic

from weed_iam.service_config import current_config
from weed_iam.util.random_fun import random_bytes_hex_encoded
from weed_iam.util.toml_util import try_load_toml, save_toml


class AppEntity(TypedDict):
    app_id: str
    app_secret: str
    name: str
    avatar: str
    callback_urls: List[str]


def _make_app_secret() -> str:
    return random_bytes_hex_encoded(4096)


class AppsRepository(Protocol):
    async def create_app(self, name: str, callback_urls: List[str]) -> Optional[AppEntity]:
        ...

    async def find_app(self, app_id: str) -> Optional[AppEntity]:
        ...

    async def list_apps(self) -> List[AppEntity]:
        ...

    async def delete_app(self, app_id: str) -> bool:
        ...

    async def reset_secret(self, app_id: str) -> bool:
        ...

    async def set_app_callback_urls(self, app_id: str, callback_urls: List[str]) -> bool:
        ...

    async def update_app(self, app_id: str, name: str, callback_urls: List[str]) -> bool:
        ...

    async def load_default_avatar(self) -> str:
        ...


class TomlAppsRepository(AppsRepository):
    apps_db: Dict[str, AppEntity]
    cached_default_avatar: Optional[str] = "/static/img/default-app-avatar.jpeg"

    def __init__(self, path: Path):
        self.apps_db = try_load_toml(path)

    async def _save_db(self):
        save_toml(current_config.apps_db_path, self.apps_db)

    async def _save_app(self, app_id: str, app: AppEntity):
        app["app_id"] = app_id
        self.apps_db[app_id] = app

        await self._save_db()

    async def create_app(self, name: str, callback_urls: List[str]) -> Optional[AppEntity]:
        name = (name or "").strip()
        if not name:
            return None

        app_id = str(uuid.uuid4())

        while app_id in self.apps_db:
            app_id = str(uuid.uuid4())

        app: AppEntity = {
            "app_id": app_id,
            "app_secret": _make_app_secret(),
            "callback_urls": callback_urls,
            "avatar": await self.load_default_avatar(),
            "name": name
        }

        self.apps_db[app_id] = app
        await self._save_db()

        return app

    async def find_app(self, app_id: str) -> Optional[AppEntity]:
        return self.apps_db.get(app_id, None)

    async def list_apps(self) -> List[AppEntity]:
        return list(self.apps_db.values())

    async def delete_app(self, app_id: str) -> bool:
        self.apps_db.pop(app_id)
        await self._save_db()

        return True

    async def reset_secret(self, app_id: str) -> bool:
        if app := await self.find_app(app_id):
            app["app_secret"] = _make_app_secret()
            await self._save_app(app_id, app)

            return True

        return False

    async def set_app_callback_urls(self, app_id: str, callback_urls: List[str]) -> bool:
        if app := await self.find_app(app_id):
            app["callback_urls"] = callback_urls
            await self._save_app(app_id, app)

            return True

        return False

    async def update_app(self, app_id: str, name: str, callback_urls: List[str]) -> bool:
        if app := await self.find_app(app_id):
            app["name"] = name
            app["callback_urls"] = callback_urls

            await self._save_app(app_id, app)

            return True

        return False

    async def load_default_avatar(self) -> str:
        if self.cached_default_avatar:
            return self.cached_default_avatar

        path = current_config.static_content_path / "img" / "default-app-avatar.jpeg"

        with path.open("rb") as fp:
            data = fp.read()

        data_string = base64.b64encode(data).decode(sys.getdefaultencoding())
        mime_type = magic.from_buffer(data[:64], mime=True)
        data_url = f"data:{mime_type};base64,{data_string}"

        self.cached_default_avatar = data_url

        return data_url
