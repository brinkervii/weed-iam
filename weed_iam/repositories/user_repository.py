import itertools
import logging
import math
import time
from pathlib import Path
from typing import Protocol, Optional, Dict, Tuple, List

from jose import jwt
from typing_extensions import TypedDict

from weed_iam import passwords, rbac
from weed_iam.pagination import Page, PageOut, PageStats
from weed_iam.passwords import hash_password
from weed_iam.service_config import current_config
from weed_iam.util.toml_util import try_load_toml, save_toml

log = logging.getLogger(__name__)


class UserEntity(TypedDict):
    user_id: str
    password: str
    hash_algorithm: str
    roles: List[str]
    app_grants: List[str]


class CookieClaims(TypedDict):
    sub: str
    iss: str
    iat: int
    exp: int


def _session_exp():
    return int(time.time()) + 1800  # 30 minutes


class UserRepository(Protocol):
    async def count_users(self) -> int:
        ...

    async def find_user(self, user_id: str) -> Optional[UserEntity]:
        ...

    async def create_user(self, email_address: str, plaintext_password: str, roles: List[str]) -> Optional[UserEntity]:
        ...

    async def login_make_cookie(self, user_id: str, plaintext_password: str) -> Optional[str]:
        ...

    async def session_token_is_valid(self, session_token: Optional[str]) -> Tuple[bool, Optional[CookieClaims]]:
        ...

    async def extend_session_token(self, session_token: str) -> Optional[str]:
        ...

    async def count_roles(self) -> int:
        ...

    async def count_permissions(self) -> int:
        ...

    async def list_users(self, page: Page) -> PageOut[UserEntity]:
        ...

    async def append_application_grant(self, user_id: str, application_id: str) -> bool:
        ...

    async def find_root_users(self) -> List[UserEntity]:
        ...


class TomlUserRepository(UserRepository):
    user_db: Dict

    def __init__(self, path: Path):
        self.user_db = try_load_toml(path)

    async def _save_db(self):
        save_toml(current_config.user_db_path, self.user_db)

    async def _save_user(self, user_id: str, user: UserEntity):
        user["user_id"] = user_id
        self.user_db[user_id] = user

        await self._save_db()

    async def find_user(self, user_id: str) -> Optional[UserEntity]:
        user_id = user_id.lower()

        if user_id in self.user_db:
            db_user = self.user_db.get(user_id)

            return {
                "user_id": user_id,
                "password": db_user["password"],
                "hash_algorithm": db_user['hash_algorithm'],
                "roles": db_user.get("roles", None) or [],
                "app_grants": db_user.get("app_grants", None) or []
            }

        return None

    async def create_user(self, email_address: str, plaintext_password: str, roles: List[str]) -> Optional[UserEntity]:
        password_hash = hash_password(plaintext_password)

        user: UserEntity = {
            "user_id": email_address,
            "password": password_hash.hash,
            "hash_algorithm": password_hash.hash_algorithm,
            "roles": roles,
            "app_grants": []
        }

        await self._save_user(user["user_id"], user)

        return user

    async def login_make_cookie(self, user_id: str, plaintext_password: str) -> Optional[str]:
        user = await self.find_user(user_id)

        if user is None:
            log.warning(f"{self.login_make_cookie} :: User not found: {user_id}")
            return None

        password_ok = passwords.verify_password(
            plaintext_password,
            user["password"],
            user["hash_algorithm"]
        )

        if not password_ok:
            log.warning(f"{self.login_make_cookie} :: Invalid password for user {user_id}")
            return None

        claims: CookieClaims = {
            "sub": user["user_id"],
            "iat": int(time.time()),
            "exp": _session_exp(),
            "iss": current_config.jwt_issuer
        }

        token = jwt.encode(claims, current_config.jwt_secret, current_config.jwt_algorithm)

        return token

    async def session_token_is_valid(self, session_token: Optional[str]) -> Tuple[bool, Optional[CookieClaims]]:
        if session_token is None:
            return False, None

        try:
            claims: CookieClaims = jwt.decode(session_token, current_config.jwt_secret,
                                              [current_config.jwt_algorithm])

        except Exception as e:
            log.error(f"{self.session_token_is_valid} :: {e}")
            return False, None

        if not claims:
            return False, None

        for k in CookieClaims.__annotations__.keys():
            if k not in claims:
                return False, None

        if int(time.time()) >= claims["exp"]:
            return False, None

        return True, claims

    async def extend_session_token(self, session_token: str) -> Optional[str]:
        claims: CookieClaims = jwt.decode(session_token, current_config.jwt_secret, [current_config.jwt_algorithm])

        if "exp" not in claims:
            return None

        now = int(time.time())
        if now - 600 >= claims["exp"]:
            claims["exp"] = _session_exp()

            return jwt.encode(claims, current_config.jwt_secret, current_config.jwt_algorithm)

        return None

    async def count_users(self) -> int:
        return len(self.user_db)

    async def count_roles(self) -> int:
        return len(rbac.roles)

    async def count_permissions(self) -> int:
        return len(rbac.all_permissions)

    async def list_users(self, page: Page) -> PageOut[UserEntity]:
        sl_start = page.index * page.offset
        sl_end = sl_start + page.offset

        user_slice = itertools.islice(self.user_db.items(), sl_start, sl_end)
        page_data = []

        for user_id, user in user_slice:
            page_data.append({
                **user,
                "user_id": user_id
            })

        total_items = await self.count_users()
        total_pages = int(math.ceil(total_items / (page.offset or 50)))

        return PageOut(
            page=page,
            stats=PageStats(
                total_items=total_items,
                total_pages=total_pages
            ),
            data=page_data
        )

    async def append_application_grant(self, user_id: str, application_id: str) -> bool:
        user = await self.find_user(user_id)
        if not user:
            return False

        grants = user.get("app_grants", None) or []
        grants = list({*grants, application_id})

        user["app_grants"] = grants

        await self._save_user(user_id, user)
        return True

    async def find_root_users(self) -> List[UserEntity]:
        return [user for user in self.user_db.values() if "root" in user["roles"]]
