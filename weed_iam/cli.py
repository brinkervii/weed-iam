import sys
from pathlib import Path

import click
import toml

from weed_iam import passwords

users_path = Path("./db/users.toml").resolve()


@click.group()
def cli():
    ...


@cli.command()
def make_user():
    if users_path.exists():
        print(f"Loading users db {users_path}")
        with users_path.open("r") as fp:
            users_db = toml.load(fp)

    else:
        print(f"Creating new users db at {users_path}")
        users_db = {}

    roles = []

    while not roles:
        click.echo("The user has no roles.")
        roles = list(
            filter(
                None,
                map(
                    str.strip,
                    click.prompt("Enter the roles for the new user").split(",")
                )
            )
        )

    user_id = click.prompt("Enter a user_id for the new user")
    password = click.prompt("Enter a password for the new user")
    password_repeat = click.prompt("Repeat the password")

    if user_id in users_db:
        print("User already exists, exiting")
        return sys.exit(-1)

    if password != password_repeat:
        print("Passwords do not match, exiting")
        return sys.exit(-1)

    pw_hash = passwords.hash_password(password)

    users_db[user_id] = {
        "hash_algorithm": pw_hash.hash_algorithm,
        "password": pw_hash.hash,
        "roles": ["user"],
        "app_grants": []
    }

    with users_path.open("w+") as fp:
        toml.dump(users_db, fp)


if __name__ == "__main__":
    cli()
