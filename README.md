# Weed IAM

Weed IAM is an Identity and Access Management (IAM) system built for development purposes. Please note that Weed IAM
should never be used in a production environment. The publisher of this code cannot be held liable for any problems that
might arise from using Weed IAM in production.

## Project status

This project is still under development and some features might be missing or broken.

## Tech Stack

Weed IAM is built using the following technologies:

- Python
- FastAPI
- Bootstrap
- HTMX

## Development Setup

To set up Weed IAM for development on your local machine, follow these steps:

1. Clone the repository:

```bash
git clone https://github.com/your-username/weed-iam.git
```

2. Navigate to the project directory:

```bash
cd weed-iam
```

3. Create a virtual environment:

```bash
python -m venv venv
   ```

4. Activate the virtual environment:

- On Windows:

```bash
venv\Scripts\activate
```

- On macOS and Linux:

```bash
source venv/bin/activate
```

5. Install the required dependencies:

```bash
pip install -r requirements.txt
```

## Running the Application

To run Weed IAM on your local machine, follow these steps:

1. Ensure that you have activated the virtual environment as described in the Development Setup section.

2. Start the Hypercorn server:

```bash
hypercorn weed_iam.app:app --reload
```

3. The Weed IAM application should now be running at [http://localhost:8000](http://localhost:8000).

## Getting Started

To get started with Weed IAM, follow these steps:

1. Open your web browser and navigate to [http://localhost:8000](http://localhost:8000).

2. You will be presented with the Weed IAM landing page. If you do not yet have a root user in the users database, Weed
   IAM will run in setup mode. Navigate to `/register` to create a root/admin account. Once you have created an account,
   log in.

3. Once logged in, you can manage users, roles, and permissions using the provided interfaces.
